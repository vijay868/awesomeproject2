import React, { Component } from 'react';
import { Text, ScrollView, TouchableNativeFeedback, View, Image } from 'react-native';
import axios from 'axios';
import PhotoSection from './photo-section';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class PhotoFeed extends Component {
  state = { photos: [] };

  constructor() {
    super();
    this.like = false;
    this.state = { heartIcon: 'ios-heart-outline', photos: [] };
  }

  componentDidMount() {
    axios.get('http://192.168.0.7/myinstagram/api/photos/all')
      .then(response => {
        this.setState({ photos: response.data });
      });
  }

  toggleLike() {
    this.like = !this.like;
    if (this.like) {
      this.setState({ heartIcon: 'ios-heart' });
    } else {
      this.setState({ heartIcon: 'ios-heart-outline' });
    }
  }

  getPhotos() {
    return this.state.photos.map(photo => {
      // return (
      //   <PhotoSection key={_photo.id} photo={_photo} />
      // );
      // return (
      //   <View key={_photo.id}>
      //     <Image 
      //       style={{width: null, height: 300}}
      //       source={{uri: _photo.image}} />
      //   </View>
      // )
      return (
        <View style={styles.container} key={photo.id}>
          <View style={styles.thumbnailSection}>
            <Image
              style={styles.thumbnail}
              source={{ uri: photo.user_avatar }} />
            <View style={styles.userContainer}>
              <Text style={styles.username}>{photo.username}</Text>
            </View>
          </View>
          <View>
            <Image
              style={{ width: null, height: 300, borderRadius: 15 }}
              source={{ uri: photo.image }} />
          </View>
          <View style={styles.heartContainer}>
          <Ionicons
                  name={this.state.heartIcon}
                  size={30}
                  style={{ color: this.state.heartIcon === 'ios-heart' ? 'red' : 'black' }}>
                </Ionicons>
          </View>
          <View style={styles.imageMeta}>
            <Text style={styles.username}>{photo.username}</Text>
            <Text>{photo.caption}</Text>
          </View>
        </View>
      );
    })
  }

  render() {
    return (
      <ScrollView>
        {this.getPhotos()}
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    margin: 10
  },

  thumbnailSection: {
    flexDirection: 'row',
    padding: 5,
    borderBottomColor: '#ddd',
    borderBottomWidth: 0,
  },

  thumbnail: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },

  userContainer: {
    justifyContent: 'center',
    padding: 5
  },

  imageMeta: {
    flexDirection: 'row'
  },

  username: {
    fontWeight: 'bold',
    paddingRight: 5,
  },

  heartContainer: {
    paddingTop: 10,
    paddingBottom: 10
  }
}