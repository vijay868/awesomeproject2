import React, { Component } from 'react';
import { View, Text, Image, TouchableNativeFeedback } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class PhotoSection extends Component {

  constructor() {
    super();
    this.like = false;
    this.state = { heartIcon: 'ios-heart-outline' };
  }

  toggleLike() {
    this.like = !this.like;
    if (this.like) {
      this.setState({ heartIcon: 'ios-heart' });
    } else {
      this.setState({ heartIcon: 'ios-heart-outline' });
    }
  }

  render() {
    return (
      <View>
          <Image
            style={{ width: null, height: 300 }}
            source={{ uri: this.props.photo.image }} />
        </View>
    );
  }
}

const styles = {
  container: {
    margin: 10
  },

  thumbnailSection: {
    flexDirection: 'row',
    padding: 5,
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
  },

  thumbnail: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },

  userContainer: {
    justifyContent: 'center',
    padding: 5
  },

  imageMeta: {
    flexDirection: 'row'
  },

  username: {
    fontWeight: 'bold',
    paddingRight: 5,
  },

  heartContainer: {
    paddingTop: 10,
    paddingBottom: 10
  }
}

